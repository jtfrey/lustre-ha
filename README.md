# lustre-ha

This project represents a simplified — though still somewhat sophisticated — service starter for use on our Lustre servers.  RedHat Cluster (with corosync et al.) is overkill, but trying to tier such things in systemd has proven that it's not really up to the task, either.

This single Python script reads an H.A. configuration file (in YAML format) that describes the resource cluster.  Each resource must have a type (kmod, zpool, lustre) and may optionally provide a list of other resources on which it depends.  Some types may have additional keyed-values permissible.

The script operates in one of two modes, **primary** and **failover**, with an operation of **start**, **stop**, or **status**.  It is easily wrapped in a systemd oneshot service (see the *systemd* directory for examples).

## Configuration file

As mentioned, the configuration file is in YAML format.  The configuration consists of a dictionary with a single top-level key, `cluster`.  The value for `cluster` is a dictionary containing:

* `resources`
* `nodes`
* `defaults` [optional]

The value of each of these keys is a dictionary, with a format discussed in the following subsections.  By default the script reads a configuration file at `/etc/lustre-ha.conf`, but the `--conf` option may be used to indicate an alternate file.

### Resources

The `resources` dictionary contains keys that are the unique identifiers (ids) of the resources.  A resource's id is used to refer to it throughout the rest of the configuration.  The value associated with the id is a dictionary, minimally containing the `type` of the resource:

* `kmod`: the resource is a dynamically-loadable kernel module
* `zpool`: the resource is a ZFS storage pool that backs MGT/MDT/OST device(s)
* `lustre`: the resource is an MGT/MDT/OST device

An optional key that may be present on any resource is `depends`, whose value is an ordered list of other resource ids which must be started before or stopped after the resource in question.  Note that the configured order is the order used by the script.  The script will perform some sanity checks:  looking for cyclic dependencies and undefined resource ids.

The generic YAML form for a resource looks like:

```
<resource-id>:
    type: <resource-type>
    depends:
      - <resource-id-1>
      - <resource-id-2>
          :
      - <resource-id-n>
```

with the `depends` key being optional.

#### kmod resources

The resource id is used as the name of the kernel module to be loaded, unless the `modulename` key is present in the resource dictionary:

```
<resource-id>:
    type: kmod
    depends:
      - <resource-id-1>
      - <resource-id-2>
          :
      - <resource-id-n>
    modulename: <kernel-module-name>
```

As an example, the zfs kernel module depends on no other resources and could be configured as:

```
zfs:
    type: kmod
```

in which case the `modulename` is inferred to be `zfs`.  An equally viable description of the resource would be:

```
zfs_kmod:
    type: kmod
    modulename: zfs
````

Resource status is checked by locating the `modulename` in `/proc/modules`.

The resource is started by doing a `modprobe` for the `modulename`.

The service will always succeed at stopping because it doesn't do anything (the kernel module is NOT unloaded).

#### zpool resources

The resource id is used as the zpool device name, unless the `device` key is present in the resource dictionary:

```
<resource-id>:
    type: zpool
    depends:
      - <resource-id-1>
      - <resource-id-2>
          :
      - <resource-id-n>
    device: <zpool-name|zpool-id>
```

It is likely that each zpool device should depend on the zfs kernel module, for example:

```
primary_zpool:
    type: zpool
    device: pool1
    depends:
      - zfs_kmod
```

Since the pool requires zfs to be active in the kernel, the dependency ensures the zfs kernel module is loaded *before* this service can be started.

Resource status is checked by doing a 'zpool list' on the device.

The resource is started by checking the state of the pool using `zpool import -d <dir>` to ensure that the desired pool is visible and importable, followed by a `zfs import` of the pool.

The service is stopped by doing a `zfs export` of the pool.

#### lustre resources

The resource id typically represents the ZFS filesystem that backs the Lustre component device (MGT, MDT, OST).  This can be overridden using the `device` key.  Unless overridden with the `mountpoint` key, the mountpoint is inferred from the portion of the id trailing the pool name as appended to the default Lustre mounting directory.  For example, with id `ost0pool/ost0` and a Lustre mounting directory of `/lustre`, the mountpoint inferred would be `/lustre/ost0`.  Any special mounting options can be provided as a string keyed with `mountopts` that will be passed to the `mount` command preceded by the `-o` flag.

```
<resource-id>:
    type: lustre
    depends:
      - <resource-id-1>
      - <resource-id-2>
          :
      - <resource-id-n>
    device: <backing-zfs-fs>
    mountpoint: <directory>
    mountopts: <string-of-mount-options>
```

As for the zpool example, each Lustre component device resource should depend on the lustre kernel module.  For example:

```
metadata/mgt:
    type: lustre
    depends:
      - metadata
      - lustre_kmod
    device: metadata/mgt
    mountpoint: /lustre/mgt
    mountopts: ""
```

The configuration fragment above illustrates the importance of ordering in the dependency list.  Dependencies will be started in the order they occur in the configuration, and will be stopped in reverse order.  The Lustre kernel module dependency is last because we only want to load that once ZFS has been fully loaded into the kernel and the pool backing the device has been started.

Resource status is checked by confirming the mountpoint exists and is a directory, and `/proc/mounts` contains a line with that resource's device, mountpoint, and type "lustre".

The resource is started by creating the mountpoint (and all parent directories) if it does not exist and issuing a `mount` command of the form:

```
mount -t lustre {-o <string-of-mount-options>} <backing-zfs-fs> <mountpoint>
```

The resource is stopped by issuing a `umount <mountpoint>` command.

### Nodes

The `nodes` dictionary contains keys that are the unique identifiers (ids) of the nodes that service the resources.  The ids should refer to the short-form hostname of the systems in question:  e.g. for a system with hostname `r02mds0.localdomain.hpc.udel.edu` the id would be `r02mds0`.  The script uses the short-form of the `nodename` field returned by `uname()` to identify the resources serviced by a system.

Each node dictionary can have two keys:

* `primary`: a list of resource ids "owned" by this node
* `failover`: a list of resource ids for which this node will function under failure of its primary node

Order of the resource ids is important:  the order in which they are specified is the order in which they will be started (and reverse of the order in which they will be stopped).

## Usage

```
usage: lustre-ha [-h] [--verbose] [--quiet] [--conf <path>]
                 [--state-dir <path>] [--mode <primary|failover>]
                 [--op <start|stop|status>] [--node <hostname>] [--summary]
                 [--simulate]

Lustre H.A. resource start/stop

optional arguments:
  -h, --help            show this help message and exit
  --verbose, -v         increase the amount of information displayed
  --quiet               decrease the amount of information displayed
  --conf <path>, -c <path>
                        path to the configuration file
  --state-dir <path>, -r <path>
                        path to the directory in which we should write state
                        files
  --mode <primary|failover>, -m <primary|failover>
                        process resources for this mode of the node
  --op <start|stop|status>, -o <start|stop|status>
                        what action to perform against the chosen mode of the
                        node
  --node <hostname>, -n <hostname>
                        masquerade as the given hostname
  --summary             display a summary of the resources and their
                        interdependencies
  --simulate            do not effect any change, merely simulate behavior
```

### Examples

See the [example configuration file](etc/lustre-ha.conf) included in this repository.

